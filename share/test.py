from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

athenaCommonFlags.FilesInput = [
    "/coepp/cephfs/mel/edawe/private/ftk/"
    "valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.digit.RDO_FTK.e4993_s2887_r8937_r9119/RDO_FTK.10733709._000019.pool.root.1"]

include('FTK2EVERYTHING.py')

# defined in TrigFTK/FTK_DataProviderSvc/share/FTK_DataProviderLoad.py
#ToolSvc.InDetTrigTrackFitterFTK.OutputLevel = VERBOSE
#ToolSvc.TrigFTK_ParticleCreatorTool.OutputLevel = DEBUG

#ToolSvc.TrackStateOnSurfaceDecorator.OutputLevel = VERBOSE
#ToolSvc.FTK_TrackStateOnSurfaceDecorator.OutputLevel = VERBOSE
#ToolSvc.FTK_Refit_TrackStateOnSurfaceDecorator.OutputLevel = VERBOSE

# defined in FTK2EVERYTHING.py
#FTKTrackCnvAlg.OutputLevel = DEBUG
#FTKRefitTrackCnvAlg.OutputLevel = DEBUG
#FTKVertexCnvAlg.OutputLevel = DEBUG
#FTKRefitVertexCnvAlg.OutputLevel = DEBUG

#from AthenaCommon.AppMgr import ServiceMgr

#ServiceMgr.MessageSvc.OutputLevel = DEBUG
#ServiceMgr.MessageSvc.verboseLimit = 9999999
#ServiceMgr.MessageSvc.debugLimit = 9999999
#ServiceMgr.MessageSvc.warningLimit = 9999999
#ServiceMgr.MessageSvc.infoLimit = 9999999
#ServiceMgr.MessageSvc.errorLimit = 9999999
#ServiceMgr.StoreGateSvc.Dump = True
#ServiceMgr.AthenaSealSvc.OutputLevel = DEBUG

#from AthenaCommon.DetFlags import DetFlags

#DetFlags.Print()

#from AthenaCommon.KeyStore import CfgKeyStore

#theCKS = CfgKeyStore("KeyStore")
#print "After addition of FTK collections ESD Output list is :"
#print '\n'.join(theCKS.streamESD())
#print "After addition of FTK collections AOD Output list is :"
#print '\n'.join(theCKS.streamAOD())
#print "objkeystore:"
#print objKeyStore
