#ifndef FTKxAODReader_H
#define FTKxAODReader_H

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include <EventLoop/Algorithm.h>

#ifndef __MAKECINT__
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODTruth/TruthParticle.h"
#endif

class FTKxAODReader : public EL::Algorithm
{
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
  public:

  FTKxAODReader(bool truth=true);

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob(EL::Job& job);
  virtual EL::StatusCode fileExecute();
  virtual EL::StatusCode histInitialize();
  virtual EL::StatusCode changeInput(bool firstfile);
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode postExecute();
  virtual EL::StatusCode finalize();
  virtual EL::StatusCode histFinalize();

  // Everything in the header file  that refers to the xAOD edm needs
#ifndef __MAKECINT__
  bool passTrackQualitySelection(const xAOD::TrackParticle*);
  bool passIP_Selection(const xAOD::TrackParticle*, const xAOD::Vertex*);
  void AddBranches(TTree* tree);
  void ResetBranches();
  bool isAcceptedTruth(const xAOD::TruthParticle* tp);
#endif

  bool m_truth; //!
  bool m_simulation; //!
  bool m_tsos_offline; //!
  bool m_tsos_ftk; //!
  xAOD::TEvent *m_event;       //!
  unsigned int m_eventCounter; //!
  TTree *tree; //!

  int event_number, run_number, truth_track_n, reco_track_n, ftk_track_n, ftk_refit_track_n;

  int truth_track_reco_match[5000],
      truth_track_ftk_match[5000],
      truth_track_ftk_refit_match[5000];

  float truth_track_reco_match_prob[5000],
        truth_track_ftk_match_prob[5000],
        truth_track_ftk_refit_match_prob[5000];

  float truth_track_pt[5000],
        truth_track_eta[5000],
        truth_track_phi[5000],
        truth_track_theta[5000],
        truth_track_d0[5000],
        truth_track_z0[5000],
        truth_track_z0st[5000],
        truth_track_qop[5000],
        truth_track_prod_perp[5000];

  int truth_track_charge[5000],
      truth_track_pdgid[5000],
      truth_track_status[5000],
      truth_track_barcode[5000];

  float reco_track_pt[5000],
        reco_track_eta[5000],
        reco_track_phi[5000],
        reco_track_theta[5000],
        reco_track_d0[5000],
        reco_track_z0[5000],
        reco_track_qop[5000],
        reco_track_chi2ndof[5000];

  int reco_track_charge[5000];

  float ftk_track_pt[5000],
        ftk_track_eta[5000],
        ftk_track_phi[5000],
        ftk_track_theta[5000],
        ftk_track_d0[5000],
        ftk_track_z0[5000],
        ftk_track_qop[5000],
        ftk_track_chi2ndof[5000];

  int ftk_track_charge[5000];

  float ftk_refit_track_pt[5000],
        ftk_refit_track_eta[5000],
        ftk_refit_track_phi[5000],
        ftk_refit_track_theta[5000],
        ftk_refit_track_d0[5000],
        ftk_refit_track_z0[5000],
        ftk_refit_track_qop[5000],
        ftk_refit_track_chi2ndof[5000];

  int ftk_refit_track_charge[5000];

  int reco_track_npixhit[5000], reco_track_nscthit[5000];
  int ftk_track_npixhit[5000], ftk_track_nscthit[5000];
  int ftk_refit_track_npixhit[5000], ftk_refit_track_nscthit[5000];

  float reco_track_locx_pix_l1[5000], reco_track_locy_pix_l1[5000],
        reco_track_locx_pix_l2[5000], reco_track_locy_pix_l2[5000],
        reco_track_locx_pix_l3[5000], reco_track_locy_pix_l3[5000],
        reco_track_locx_pix_l4[5000], reco_track_locy_pix_l4[5000];

  float reco_track_locx_sct_l1[5000], reco_track_locx_sct_l2[5000],
        reco_track_locx_sct_l3[5000], reco_track_locx_sct_l4[5000],
        reco_track_locx_sct_l5[5000];

  float ftk_track_locx_pix_l1[5000], ftk_track_locy_pix_l1[5000],
        ftk_track_locx_pix_l2[5000], ftk_track_locy_pix_l2[5000],
        ftk_track_locx_pix_l3[5000], ftk_track_locy_pix_l3[5000],
        ftk_track_locx_pix_l4[5000], ftk_track_locy_pix_l4[5000];

  float ftk_track_locx_sct_l1[5000], ftk_track_locx_sct_l2[5000],
        ftk_track_locx_sct_l3[5000], ftk_track_locx_sct_l4[5000],
        ftk_track_locx_sct_l5[5000];

  float ftk_refit_track_locx_pix_l1[5000], ftk_refit_track_locy_pix_l1[5000],
        ftk_refit_track_locx_pix_l2[5000], ftk_refit_track_locy_pix_l2[5000],
        ftk_refit_track_locx_pix_l3[5000], ftk_refit_track_locy_pix_l3[5000],
        ftk_refit_track_locx_pix_l4[5000], ftk_refit_track_locy_pix_l4[5000];

  float ftk_refit_track_locx_sct_l1[5000], ftk_refit_track_locx_sct_l2[5000],
        ftk_refit_track_locx_sct_l3[5000], ftk_refit_track_locx_sct_l4[5000],
        ftk_refit_track_locx_sct_l5[5000];

  // this is needed to distribute the algorithm to the workers
  ClassDef(FTKxAODReader, 1);
};

#endif
