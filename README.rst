FTKxAODReader
=============

FTKxAODReader creates simple ROOT ntuples containing information on raw and
refit FTK tracks, offline tracks, and truth particles. These ntuples may be
used as input for making validation plots of track parameter resolution and FTK
efficiencies with respect to truth or offline tracking.

In addition to the documentation below, please refere to my slides and code
on Indico here: https://indico.cern.ch/event/468480/ and the documentation
on the Twiki: https://twiki.cern.ch/twiki/bin/view/Atlas/FTKSoftwareRDO2xAOD

Getting FTKxAODReader
---------------------

Get the latest version of FTKxAODReader by cloning with git::

   git clone https://gitlab.cern.ch/end/FTKxAODReader.git


Generating DAOD_FTK from RDO_FTK
--------------------------------

Use the provided job options in ``FTKxAODReader/share/FTK2EVERYTHING.py``
to produce ESD, AOD, and DAOD_FTK from RDO_FTK as follows::

   cd FTKxAODReader/share
   setupATLAS --quiet
   asetup AtlasProduction,21.0.19.1,here
   voms-proxy-init -voms atlas
   lsetup panda
   pathena FTK2EVERYTHING.py \
      --nFilesPerJob 1 --supStream GLOBAL \
      --inDS [something].recon.RDO.[something] \
      --outDS user.[USER].[something]

This will create separate output datasets for each output type (log, ESD, AOD,
and DAOD_FTK). The DAOD_FTK output container will be the input for
FTKxAODReader.

Running FTKxAODReader on DAOD_FTK
---------------------------------

Set up RootCore in a fresh terminal while in the directory containing
FTKxAODReader::

   setupATLAS --quiet
   rcSetup Base,2.4.28
   rc build
   voms-proxy-init -voms atlas
   lsetup panda
   ftk2ntup -g user.[USER].[SOMETHING]_NTUP user.[USER].[SOMETHING]_DAOD_FTK [OUTPUT_DIRECTORY]

After the grid jobs have finished, the output should be present in the local
output directory.

Another option is to download the DAOD_FTK container and run without the ``-g``
option (replace the input dataset with the directory containing the DAOD_FTK).

Making Plots
------------

**WORK IN PROGRESS**

A script that creates plots of track parameters, efficiencies, and resolutions
is provided by FTKxAODReader. This python script depends on a few python
packages, as well as ROOT. First, if you don't have pip installed, do the
following::

   curl -O https://bootstrap.pypa.io/get-pip.py
   python get-pip.py --user

Then install the following python packages::

   pip install numpy matplotlib rootpy root_numpy

Now you should be able to run the plot-ftk script on the output ntuple from
FTKxAODReader::

   ftkplot /path/to/ntuple

Plans
-----

* Migrate plotting code from TrigFTKSim's efficiency code to FTKxAODReader
* Create validation plots along with ntuple
